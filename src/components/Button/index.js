import React from 'react';
import PropTypes from 'prop-types';

const Button = ({ onClick, title, className }) =>
  <button className={className} onClick={onClick}>
    {title}
  </button>;

Button.propTypes = {
  onClick: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
};

export default Button;
