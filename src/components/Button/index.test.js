import React from 'react';
import { shallow } from 'enzyme';

import Button from './index';

describe('Button', () => {
  let props;
  let button;
  beforeEach(() => {
    props = {
      onClick: jest.fn(),
      title: 'Onion',
      className: 'test',
    };
    const wrapper = shallow(<Button {...props} />);
    button = wrapper.find('button');
  });

  it('renders button', () => {
    expect(button.length).toBe(1);
  });

  it('has proper className', () => {
    expect(button.hasClass('test')).toBe(true);
  });

  it('has proper title', () => {
    expect(button.text()).toEqual('Onion');
  });

  it('fires passed function on click', () => {
    button.simulate('click');
    expect(props.onClick).toHaveBeenCalled();
  });
});
