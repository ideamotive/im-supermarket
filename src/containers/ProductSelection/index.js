import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { addProduct } from '../../reducers/cart.js';
import { getProductCodes, getProductName } from '../../data/products';
import Button from '../../components/Button';
import './styles.css';

export class ProductSelection extends Component {
  handleButtonClick = (e, productCode) => {
    e.preventDefault();
    this.props.addProduct({ productCode });
  };

  render() {
    return (
      <div className="product-selection">
        {getProductCodes().map((productCode, idx) =>
          <Button
            key={idx}
            className="product-selection__button"
            onClick={e => this.handleButtonClick(e, productCode)}
            title={getProductName(productCode)}
          />
        )}
      </div>
    );
  }
}

ProductSelection.propTypes = {
  addProduct: PropTypes.func.isRequired,
};

export const mapDispatchToProps = dispatch => ({
  addProduct: productCode => dispatch(addProduct(productCode)),
});

export default connect(null, mapDispatchToProps)(ProductSelection);
