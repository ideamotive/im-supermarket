import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { formatCurrency } from '../../data/products';
import { getTotal } from '../../reducers/cart';
import './styles.css';

export class CartTotal extends Component {
  render() {
    return (
      <div className="cart-total">
        {formatCurrency(this.props.total)}
      </div>
    );
  }
}

CartTotal.propTypes = {
  total: PropTypes.number.isRequired,
};

const mapStateToProps = state => ({ total: getTotal(state) });

export default connect(mapStateToProps)(CartTotal);
