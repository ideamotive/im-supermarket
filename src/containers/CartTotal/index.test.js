import React from 'react';
import { shallow } from 'enzyme';

import { CartTotal } from './index';

describe('CartTotal', () => {
  let wrapper;
  beforeEach(() => {
    const props = {
      total: 11.41,
    };
    wrapper = shallow(<CartTotal {...props} />);
  });

  it('renders component with correct price', () => {
    const formattedTotal = '$11.41';
    expect(wrapper.text()).toBe(formattedTotal);
  });
});
