import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { getProducts } from '../../reducers/cart.js';
import { getProductName, getProductValue, formatCurrency } from '../../data/products';
import './styles.css';

class CartItems extends Component {
  render() {
    return (
      <div className="cart-items">
        <ul>
          {this.props.products.map((productCode, idx) =>
            <li key={idx}>
              <span className="cart-items__name">
                {getProductName(productCode)}
              </span>
              <span>
                {formatCurrency(getProductValue(productCode))}
              </span>
            </li>
          )}
        </ul>
      </div>
    );
  }
}

CartItems.propTypes = {
  products: PropTypes.arrayOf(PropTypes.string).isRequired,
};

const mapStateToProps = state => ({
  products: getProducts(state),
});

export default connect(mapStateToProps)(CartItems);
