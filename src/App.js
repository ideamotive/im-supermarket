import React, { Component } from 'react';

import ProductSelection from './containers/ProductSelection';
import CartItems from './containers/CartItems';
import CartTotal from './containers/CartTotal';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="app">
        <div className="app__header">
          <h2>Ideamotive</h2>
        </div>
        <p className="app__intro">Add products:</p>
        <ProductSelection />
        <CartItems />
        <CartTotal />
      </div>
    );
  }
}

export default App;
