import { getProductCodes, getProductName, getProductValue, formatCurrency } from './products';

describe('product helpers', () => {
  describe('getProductCodes', () => {
    it('returns all product codes', () => {
      expect(getProductCodes()).toEqual(['BAN', 'CHE', 'ONI']);
    });
  });

  describe('getProductName', () => {
    it('returns correct name for code', () => {
      expect(getProductName('BAN')).toBe('Banana');
    });
  });

  describe('getProductValue', () => {
    it('returns correct value for code', () => {
      expect(getProductValue('CHE')).toBe(8.5);
    });
  });

  describe('formatCurrency', () => {
    it('returns number with currency', () => {
      expect(formatCurrency(8.5)).toBe('$8.50');
    });
  });
});
