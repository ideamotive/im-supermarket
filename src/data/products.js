export const products = {
  CHE: { value: 8.5, name: 'Cheese' },
  BAN: { value: 1.58, name: 'Banana' },
  ONI: { value: 1.33, name: 'Onion' },
};

export const getProductCodes = () => Object.keys(products).sort();

export const getProductName = itemCode => products[itemCode].name;

export const getProductValue = itemCode => products[itemCode].value;

export const formatCurrency = number =>
  number.toLocaleString('en-US', { style: 'currency', currency: 'USD' });
