import reducer, { ADD_PRODUCT, addProduct, getProducts, getTotal } from './cart';

describe('reducer', () => {
  it('returns initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      products: [],
    });
  });

  describe('ADD_PRODUCT', () => {
    it('adds product to products array', () => {
      const cheese = 'CHE';
      const banana = 'BAN';
      expect(
        reducer(
          { products: [cheese] },
          {
            type: ADD_PRODUCT,
            productCode: banana,
          }
        )
      ).toEqual({
        products: [cheese, banana],
      });
    });
  });
});

describe('actions', () => {
  describe('addProduct', () => {
    it('returns ADD_PRODUCT type and productCode', () => {
      const productCode = 'ONI';
      const expectedResult = {
        type: ADD_PRODUCT,
        productCode,
      };
      expect(addProduct({ productCode })).toEqual(expectedResult);
    });
  });
});

describe('selectors', () => {
  let state;
  let products;
  beforeEach(() => {
    products = ['CHE', 'BAN', 'ONI'];
    state = { products };
  });

  describe('getProducts', () => {
    it('returns all products', () => {
      expect(getProducts(state)).toEqual(products);
    });
  });

  describe('getTotal', () => {
    it('sums products correctly ', () => {
      const expectedValue = 11.41;
      expect(getTotal(state)).toEqual(expectedValue);
    });
  });
});
