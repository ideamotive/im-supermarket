# Installation
Install the dependencies and devDependencies and start the server.

```
$ yarn install
$ yarn start
```
To run tests
```
$ yarn test
```
# Problem description

You are working for a large supermarket and the cash register has just broken.
You know JavaScript and React so you can build a virtual cash register. After two coffees your application is ready and you're able to display total price for items in cart. Your boss is impressed and wants a way to include discounts.

##### Requested Features
 - If you buy 3 or more onions, the price should drop to $1.00
 - If you purchase a cheese you can get a second one at half price
 - App must show the user which discounts are being applied

# Submitting solution
Clone this repository and create private project on Bitbucket.
Before you send a solution add missing tests and make sure all tests are passing.

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
